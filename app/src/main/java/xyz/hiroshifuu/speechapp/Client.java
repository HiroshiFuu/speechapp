package xyz.hiroshifuu.speechapp;

import android.os.AsyncTask;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client extends AsyncTask<String,Void,String> {

    private String dstAddress;
    private int dstPort;
    private String dstqry;
    private String response = "";
    private TextView textResponse;
    PrintWriter out;
    private SpeechActivity mainObj;
    Socket socket = null;
    private InputStream inputStream;

    Client(String addr, int port, String query, SpeechActivity mainObj) {
        dstAddress = addr;
        dstPort = port;
        dstqry = query;
        this.mainObj = mainObj;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            connectWithServer();
            sendDataWithString(dstqry);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                    1024);
            byte[] buffer = new byte[1024];
            int bytesRead;
            /*
             * notice: inputStream.read() will block if no data return
             */
            if ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
                response += byteArrayOutputStream.toString("UTF-8");
                //buffer=null;// bufer null crashes the app after 1 execution
                //if(bytesRead==-1)
                //{buffer = new byte[1024];}
            }
            //this.textResponse.setText(response);
            //String caft=textResponse.getText().toString();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "IOException: " + e.toString();
        } finally {
            if (socket != null) {
                disConnectWithServer();
            }
        }
        return response;
    }

    private void connectWithServer() {
        try {
            if (socket == null) {
                socket = new Socket(dstAddress, dstPort);
                out = new PrintWriter(socket.getOutputStream());
                inputStream = socket.getInputStream();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void disConnectWithServer() {
        if (socket != null) {
            if (socket.isConnected()) {
                try {
                    inputStream.close();
                    out.close();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void sendDataWithString(String message) {
        if (message != null) {
            connectWithServer();
            out.write(message);
            out.flush();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        //doInBackground();
        //connectWithServer();
    }

    @Override
    protected void onPostExecute(String s) {
        mainObj.setResponse(response);
        super.onPostExecute(s);
    }
}