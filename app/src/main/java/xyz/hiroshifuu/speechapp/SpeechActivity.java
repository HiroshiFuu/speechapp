package xyz.hiroshifuu.speechapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Locale;

public class SpeechActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    private static final int SERVERPORT = 5588;
    private static final String SERVER_IP = "192.168.1.109";
    public SpeechActivity main = this;
    private TextView status_tv;
    private TextView result_tv;
    private TextView result_server_tv;
    private Button start_listen_btn;
    private SpeechRecognizerManager mSpeechManager;
    private TextToSpeech tts;
    private String bus = "No bus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.speech);
        findViews();
        setClickListeners();

        tts = new TextToSpeech(getApplicationContext(), this);
    }

    private void findViews() {
        status_tv = findViewById(R.id.status_tv);
        result_tv = findViewById(R.id.result_tv);
        result_server_tv = findViewById(R.id.result_server_tv);
        start_listen_btn = findViewById(R.id.start_listen_btn);
    }

    private void setClickListeners() {
        final Activity that = this;
        start_listen_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PermissionHandler.checkPermission(that, PermissionHandler.RECORD_AUDIO)) {
                    switch (v.getId()) {
                        case R.id.start_listen_btn:
                            if (mSpeechManager == null) {
                                SetSpeechListener();
                            } else if (!mSpeechManager.ismIsListening()) {
                                mSpeechManager.destroy();
                                SetSpeechListener();
                            }
                            status_tv.setText(getString(R.string.you_may_speak));
                            start_listen_btn.setClickable(false);
                            start_listen_btn.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                            break;
                    }
                } else {
                    PermissionHandler.askForPermission(PermissionHandler.RECORD_AUDIO, that);
                }
            }
        });
    }

    private void SetSpeechListener() {
        mSpeechManager = new SpeechRecognizerManager(this, new SpeechRecognizerManager.onResultsReady() {
            @Override
            public void onResults(ArrayList<String> results) {
                if (results != null && results.size() > 0) {
                    String res = results.get(0);
                    result_tv.setText(res);
                    Client myClient = new Client(SERVER_IP, SERVERPORT, res, main);
                    myClient.execute();
                } else {
                    status_tv.setText(getString(R.string.no_results_found));
                }
            }
        });
    }

    public void setResponse(String response) {
        result_server_tv.setText(response);
        TTS_speak(response);
        status_tv.setText(getString(R.string.destroied));
        mSpeechManager.destroy();
        mSpeechManager = null;
        start_listen_btn.setClickable(true);
        start_listen_btn.getBackground().setColorFilter(null);
    }

    @Override
    protected void onPause() {
        if (mSpeechManager != null) {
            mSpeechManager.destroy();
            mSpeechManager = null;
        }
        mSpeechManager = null;
        super.onPause();

        if (tts != null) {
            tts.shutdown();
        }
    }

    @Override
    protected void onResume() {
        tts = new TextToSpeech(getApplicationContext(), this);
        super.onResume();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            if (tts.isLanguageAvailable(Locale.UK) == TextToSpeech.LANG_AVAILABLE)
                tts.setLanguage(Locale.UK);
        } else if (status == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...",
                    Toast.LENGTH_LONG).show();
        }
        Bundle b = getIntent().getExtras();
        if (b != null)
            bus = b.getString("bus");
        TTS_speak("TTS is ready, Bus ID is : " + bus);
    }

    private void TTS_speak(String speech) {
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        int amStreamMusicMaxVol = am.getStreamMaxVolume(am.STREAM_MUSIC);
        am.setStreamVolume(am.STREAM_MUSIC, amStreamMusicMaxVol,0 );

        Bundle bundle = new Bundle();
        bundle.putInt(TextToSpeech.Engine.KEY_PARAM_STREAM, AudioManager.STREAM_MUSIC);
        bundle.putInt(TextToSpeech.Engine.KEY_PARAM_VOLUME, amStreamMusicMaxVol);

        tts.speak(speech, TextToSpeech.QUEUE_FLUSH, null, null);
    }
}
